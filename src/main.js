import { createApp } from 'vue'
import App from './App.vue'

const app = createApp(App).mount('#app')


Sentry.init({
  app,
  dsn: "https://6875a1e83300441e81882161588d246f@o1085111.ingest.sentry.io/6095587",
  integrations: [
    new Integrations.BrowserTracing({
      routingInstrumentation: Sentry.vueRouterInstrumentation(router),
      tracingOrigins: ["localhost", "my-site-url.com", /^\//],
    }),
  ],
  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});
